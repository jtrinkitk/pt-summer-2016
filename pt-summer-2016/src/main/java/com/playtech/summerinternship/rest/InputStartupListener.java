package com.playtech.summerinternship.rest;

import javax.ws.rs.ext.Provider;

import com.playtech.summerinternship.input.InputService;
import com.sun.jersey.api.model.AbstractResourceModelContext;
import com.sun.jersey.api.model.AbstractResourceModelListener;

@Provider
public class InputStartupListener implements AbstractResourceModelListener {

	@Override
	public void onLoaded(AbstractResourceModelContext context) {
		Runnable inputService = new InputService();
		new Thread(inputService).start();
	}
}
