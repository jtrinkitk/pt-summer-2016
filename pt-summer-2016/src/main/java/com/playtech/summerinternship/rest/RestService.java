package com.playtech.summerinternship.rest;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Compiles the pattern provided in the request for {@link DataCollector}. Returns a json list of {@link SearchResult}s
 * which was returned by the DataCollector. An empty list is returned if no results are found.
 */
@Path("aggr")
public class RestService {

	@Path("/query/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SearchResult> query(@QueryParam("pattern") String pattern, @QueryParam("start") long startTimestamp, @QueryParam("end") long endTimestamp) {

		try {
			Pattern compiledPattern = Pattern.compile(pattern);
			DataCollector dc = new DataCollector(compiledPattern, startTimestamp, endTimestamp);
			List<SearchResult> response = dc.collectData();
			return response;
		} catch (PatternSyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

}