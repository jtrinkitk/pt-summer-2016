package com.playtech.summerinternship.rest;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.playtech.summerinternship.input.DiskWriter;

/**
 * Finds and reads the data files that match the pattern requested by the client, discards all contents that don't match
 * the time constraints and returns a list of results.
 */

public class DataCollector {

	static final String DATA_ROOT_DIRECTORY = DiskWriter.DATA_ROOT_DIRECTORY;
	private Pattern pattern;
	private long startTimestamp;
	private long endTimestamp;

	public DataCollector(Pattern pattern, long startTimeStamp, long endTimestamp) {
		this.pattern = pattern;
		this.startTimestamp = startTimeStamp;
		this.endTimestamp = endTimestamp;
	}

	public List<SearchResult> collectData() {
		List<SearchResult> resultsList = new ArrayList<SearchResult>();

		List<Path> filePaths = new ArrayList<Path>();
		filePaths = getMatchingFilePaths(DATA_ROOT_DIRECTORY);
		for (Path filePath : filePaths) {
			List<Long[]> allDatapoints = readFromFile(filePath);
			List<Long[]> filteredDatapoints = matchTimestamp(allDatapoints);
			if (!filteredDatapoints.isEmpty()) {
				SearchResult result = new SearchResult(filteredDatapoints, dotifyFilePath(filePath));
				resultsList.add(result);
			}
		}
		return resultsList;
	}

	private List<Long[]> matchTimestamp(List<Long[]> datapoints) {
		List<Long[]> notMatching = new ArrayList<Long[]>();
		for (Long[] datapoint : datapoints) {
			long timestamp = datapoint[0];
			if (timestamp < startTimestamp || timestamp > endTimestamp) {
				notMatching.add(datapoint);
			}
		}
		datapoints.removeAll(notMatching);
		return datapoints;
	}

	private List<Long[]> readFromFile(Path filePath) {
		List<Long[]> datapoints = new ArrayList<Long[]>();

		try {
			List<String> lines = Files.readAllLines(filePath);
			for (String string : lines) {
				String[] data = string.split("\\s");
				datapoints.add(new Long[] { Long.parseLong(data[0]), Long.parseLong(data[1]) });
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return datapoints;
	}

	private boolean pathMatchesPattern(Path filePath, Pattern pattern) {
		String metricPath = dotifyFilePath(filePath);
		return pattern.matcher(metricPath).matches();
	}

	private List<Path> getMatchingFilePaths(String directoryName) {
		final List<Path> filePaths = new ArrayList<Path>();
		try {
			Path startPath = Paths.get(directoryName);
			Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(Path filePath, BasicFileAttributes attrs) {
					if (pathMatchesPattern(filePath, pattern)) {
						filePaths.add(filePath);
					}
					return FileVisitResult.CONTINUE;
				}

			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filePaths;
	}

	private String dotifyFilePath(Path filePath) {
		String dotifiedPath = filePath.toString().substring(DATA_ROOT_DIRECTORY.length(), filePath.toString().length());
		dotifiedPath = dotifiedPath.replaceAll("\\\\", ".");
		dotifiedPath = dotifiedPath.replaceAll("/", ".");
		return dotifiedPath;
	}

}
