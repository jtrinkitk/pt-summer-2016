package com.playtech.summerinternship.rest;

import java.util.List;

/**
 * Model object for the Search Results that are listed in the JSON response.
 */
public class SearchResult {

	private List<Long[]> datapoints;
	private String name;

	public SearchResult(List<Long[]> datapoints, String name) {
		this.datapoints = datapoints;
		this.name = name;
	}

	public List<Long[]> getDatapoints() {
		return datapoints;
	}

	public String getName() {
		return name;
	}
}
