package com.playtech.summerinternship.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Calculates the maximum and average of all incoming data each second and each minute. Uses {@link DiskWriter} to write
 * the aggregated data to disk.
 */
public class InputAggregator implements Runnable {

	private List<Metric> list;

	public InputAggregator(List<Metric> list) {
		this.list = list;
	}

	@Override
	public void run() {

		int i = 0; // second counter for minute based task

		Map<String, List<Metric>> oneSecondMap = new HashMap<String, List<Metric>>();
		Map<String, List<Metric>> oneMinuteMap = new HashMap<String, List<Metric>>();

		while (true) {

			for (Metric metric : list) {
				String metricPath = metric.getMetricPath();
				List<Metric> metricsList = oneSecondMap.get(metricPath);

				if (metricsList == null) {
					metricsList = new ArrayList<Metric>();
				}

				metricsList.add(metric);
				oneSecondMap.put(metricPath, metricsList);
			}

			for (String key : oneSecondMap.keySet()) {
				List<Metric> currentMetrics = oneSecondMap.get(key);
				long max = 0;
				long average = 0;
				for (Metric metric : currentMetrics) {
					max = Math.max(max, metric.getValue());
					average += metric.getValue();

					List<Metric> oneMinuteList = oneMinuteMap.get(key);
					if (oneMinuteList == null) {
						oneMinuteList = new ArrayList<Metric>();
					}

					oneMinuteList.add(metric);
					oneMinuteMap.put(key, oneMinuteList);
				}
				Metric oneSecAverage = new Metric(currentMetrics.get(0).getMetricPath(), Math.round(average / currentMetrics.size()), System.currentTimeMillis());
				Metric oneSecMaximum = new Metric(currentMetrics.get(0).getMetricPath(), max, System.currentTimeMillis());
				Runnable diskWriteAverage = new DiskWriter(oneSecAverage, "1SecondAvg");
				Runnable diskWriteMaximum = new DiskWriter(oneSecMaximum, "1SecondMax");
				new Thread(diskWriteAverage).start();
				new Thread(diskWriteMaximum).start();
			}

			if (i == 60) {
				for (String key : oneMinuteMap.keySet()) {
					List<Metric> currentMetrics = oneMinuteMap.get(key);
					long max = 0;
					long average = 0;
					for (Metric metric : currentMetrics) {
						max = Math.max(max, metric.getValue());
						average += metric.getValue();
					}
					Metric oneSecAverage = new Metric(currentMetrics.get(0).getMetricPath(), Math.round(average / currentMetrics.size()), System.currentTimeMillis());
					Metric oneSecMaximum = new Metric(currentMetrics.get(0).getMetricPath(), max, System.currentTimeMillis());
					Runnable diskWriteAverage = new DiskWriter(oneSecAverage, "1MinuteAvg");
					Runnable diskWriteMaximum = new DiskWriter(oneSecMaximum, "1MinuteMax");
					new Thread(diskWriteAverage).start();
					new Thread(diskWriteMaximum).start();
				}
				oneMinuteMap.clear();
				i = 0;
			}

			oneSecondMap.clear();

			try {
				list.clear();
				i++;
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
