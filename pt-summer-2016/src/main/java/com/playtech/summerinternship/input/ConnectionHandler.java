package com.playtech.summerinternship.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.List;

/**
 * Handles connections to the data port and parses incoming plaintext data for {@link InputAggregator}. If the incoming
 * data is valid it is added to a synchronized list which the InputAggregator uses for aggregated data generation Data
 * is validated based on its compliance with the "<metric path> <metric value> <timestamp>" format and the data's
 * timestamp's relative accuracy compared to the server's time.
 */
public class ConnectionHandler implements Runnable {

	private Socket socket;
	private List<Metric> list;
	static final int TIMESTAMP_VARIABILITY_ALLOWANCE = 5000;

	public ConnectionHandler(Socket socket, List<Metric> list) throws IOException {
		this.socket = socket;
		this.list = list;
	}

	@Override
	public void run() {
		while (!socket.isClosed()) {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String inputString;
				while ((inputString = br.readLine()) != null) {
					if (isValidMetric(inputString)) {
						String[] temp = inputString.split("\\s");
						list.add(new Metric(temp[0], Integer.parseInt(temp[1]), System.currentTimeMillis()));
					}
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isValidMetric(String inputString) {
		inputString.trim();
		if (inputString.isEmpty()) {
			return false;
		}

		String[] temp = inputString.split("\\s");
		if (temp.length > 2) {
			char[] metricPath = temp[0].toCharArray(); // check for characters that might be invalid for file creation
			for (char c : metricPath) {
				if (!Character.isLetterOrDigit(c) && c != '.') {
					return false;
				}
			}

			try {
				int value = Integer.parseInt(temp[1]); // check for valid value and timestamp
				long timeStamp = Long.parseLong(temp[2]);
				if (value >= 1 && Math.abs(timeStamp - System.currentTimeMillis()) <= TIMESTAMP_VARIABILITY_ALLOWANCE) {
					return true;
				}
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return false;
	}

}
