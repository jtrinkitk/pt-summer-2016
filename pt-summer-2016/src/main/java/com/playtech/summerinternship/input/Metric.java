package com.playtech.summerinternship.input;

/**
 * Model object for the metrics that are sent on the data connection. Note that the metric's value also uses 'long'
 * despite the examples seemingly making 'int' viable as well. As I had no further frame of reference (regarding how
 * large the metric values might get) and the datapoints returned by the API are a list of Long[] arrays anyway I chose to go with long
 * here as well.
 */
public class Metric {

	private String metricPath;
	private long value;
	private long timestamp;

	public Metric(String metricPath, long value, long timestamp) {
		this.metricPath = metricPath;
		this.value = value;
		this.timestamp = timestamp;
	}

	public String getMetricPath() {
		return metricPath;
	}

	public long getValue() {
		return value;
	}

	public long getTimestamp() {
		return timestamp;
	}

}
