package com.playtech.summerinternship.input;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Handles writing the incoming data that has been aggregated by {@link InputAggregator}. Files are written to
 * DATA_ROOT/metricpath[0]/../metricpath[n].extension
 */
public class DiskWriter implements Runnable {

	public static final String DATA_ROOT_DIRECTORY = System.getProperty("user.home") + "/pt_data/";

	private Metric metric;
	private String extension;

	public DiskWriter(Metric metric, String extension) {
		this.metric = metric;
		this.extension = extension;
	}

	@Override
	public void run() {
		StringBuilder fileLocation = new StringBuilder();
		StringBuilder content = new StringBuilder();
		String[] path = metric.getMetricPath().split("[.]");

		fileLocation.append(DATA_ROOT_DIRECTORY);
		for (int i = 0; i < path.length - 1; i++) {
			fileLocation.append(path[i]);
			fileLocation.append("/");
		}
		File directory = new File(fileLocation.toString());
		fileLocation.append(path[path.length - 1]);
		fileLocation.append(".");
		fileLocation.append(extension);
		content.append(metric.getTimestamp());
		content.append(" ");
		content.append(metric.getValue());

		File file = new File(fileLocation.toString());
		try {
			if (!directory.exists()) {
				directory.mkdirs();
			}
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content.toString());
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
