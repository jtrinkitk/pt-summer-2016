package com.playtech.summerinternship.input;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Opens the specified data port, allowing each metric to be sent in a separate connection. The connections are handled
 * by {@link ConnectionHandler}.
 */
public class InputService implements Runnable {

	private final static int PORT_NUMBER = 2003;

	@Override
	public void run() {
		ServerSocket serverSocket;

		List<Metric> list = Collections.synchronizedList(new ArrayList<Metric>());
		Runnable InputAggregator = new InputAggregator(list);
		new Thread(InputAggregator).start();
		while (true) {
			try {
				serverSocket = new ServerSocket(PORT_NUMBER);
				Socket socket = serverSocket.accept();
				Runnable connectionHandler = new ConnectionHandler(socket, list);
				new Thread(connectionHandler).start();
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
